// random number
var answer = Math.floor(Math.random() * 101);

// Hides the Restartbutton
restartGuessGame.style.display = 'none';
document.getElementById("textResponse").innerHTML = "Please enter a number...";

// Game initiation
document.getElementById("btn-Guess").onclick = function(){
    startGame();
}

// Start Game Function
function startGame() {
    // Get value from players guess
    let guess =parseInt(document.getElementById("playerGuess").value)

    // Hides the Restartbutton
    restartGuessGame.style.display = 'none';
    // Starts checking the answer
    if ( guess === answer) {
        // VICTORY
        document.getElementById("textResponse").innerHTML = "Your guess was correct!";
        // Displays the Restartbutton
        restartGuessGame.style.display = 'block';
    }
    else if ( guess < answer ){
        // TO LOW
        document.getElementById("textResponse").innerHTML = "Damn! Your guess was to low!";
    }

    else if ( guess > answer) {
        //TO HIGH
        document.getElementById("textResponse").innerHTML = "Damn! Your guess was to high!";
    }

    else {
        document.getElementById("textResponse").innerHTML = "Please enter a number...";
    }
}

// Restart game
restartGuessGame.addEventListener('click', function()  {
    answer = Math.floor(Math.random() * 101);
    startGame();
})